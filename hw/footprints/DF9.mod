PCBNEW-LibModule-V1 Sun Jul  2 19:01:24 2017
$INDEX
DF9-9-1V
DF9-11-1V
DF9-13-1V
DF9-15-1V
DF9-17-1V
DF9-19-1V
DF9-21-1V
DF9-23-1V
DF9-25-1V
DF9-31-1V
DF9-41-1V
$EndINDEX
$MODULE DF9-9-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-9-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-9-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 819 0 0 0
At SMD N 00888000
Po -787 771
$EndPAD
$PAD
Sh "2" R 276 819 0 0 0
At SMD N 00888000
Po -393 771
$EndPAD
$PAD
Sh "3" R 274 819 0 0 0
At SMD N 00888000
Po 0 771
$EndPAD
$PAD
Sh "4" R 276 819 0 0 0
At SMD N 00888000
Po 393 771
$EndPAD
$PAD
Sh "5" R 276 819 0 0 0
At SMD N 00888000
Po 787 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -1909 0
$EndPAD
$PAD
Sh "6" R 276 819 0 0 0
At SMD N 00888000
Po 590 -771
$EndPAD
$PAD
Sh "7" R 275 819 0 0 0
At SMD N 00888000
Po 196 -771
$EndPAD
$PAD
Sh "8" R 275 819 0 0 0
At SMD N 00888000
Po -196 -771
$EndPAD
$PAD
Sh "9" R 276 819 0 0 0
At SMD N 00888000
Po -590 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 1909 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -1397 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 1397 0
$EndPAD
DS -1633 826 -1633 -826 39 21
DS -1633 -826 1633 -826 39 21
DS 1633 -826 1633 826 39 21
DS 1633 826 -1633 826 39 21
DS -1988 570 -1988 -570 39 21
DS -1988 -570 -1633 -570 39 21
DS -1633 -570 -1633 570 39 21
DS -1633 570 -1988 570 39 21
DS -1633 826 -1633 -826 39 21
DS -1633 -826 1633 -826 39 21
DS 1633 -826 1633 826 39 21
DS 1633 826 -1633 826 39 21
DS 1988 570 1988 -570 39 21
DS 1988 -570 1633 -570 39 21
DS 1633 -570 1633 570 39 21
DS 1633 570 1988 570 39 21
$EndMODULE DF9-9-1V
$MODULE DF9-11-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-11-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-11-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 819 0 0 0
At SMD N 00888000
Po -984 771
$EndPAD
$PAD
Sh "2" R 276 819 0 0 0
At SMD N 00888000
Po -590 771
$EndPAD
$PAD
Sh "3" R 275 819 0 0 0
At SMD N 00888000
Po -196 771
$EndPAD
$PAD
Sh "4" R 275 819 0 0 0
At SMD N 00888000
Po 196 771
$EndPAD
$PAD
Sh "5" R 276 819 0 0 0
At SMD N 00888000
Po 590 771
$EndPAD
$PAD
Sh "6" R 276 819 0 0 0
At SMD N 00888000
Po 984 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -2106 0
$EndPAD
$PAD
Sh "7" R 276 819 0 0 0
At SMD N 00888000
Po 787 -771
$EndPAD
$PAD
Sh "8" R 276 819 0 0 0
At SMD N 00888000
Po 393 -771
$EndPAD
$PAD
Sh "9" R 274 819 0 0 0
At SMD N 00888000
Po 0 -771
$EndPAD
$PAD
Sh "10" R 276 819 0 0 0
At SMD N 00888000
Po -393 -771
$EndPAD
$PAD
Sh "11" R 276 819 0 0 0
At SMD N 00888000
Po -787 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 2106 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -1594 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 1594 0
$EndPAD
DS -1830 826 -1830 -826 39 21
DS -1830 -826 1830 -826 39 21
DS 1830 -826 1830 826 39 21
DS 1830 826 -1830 826 39 21
DS -2185 570 -2185 -570 39 21
DS -2185 -570 -1830 -570 39 21
DS -1830 -570 -1830 570 39 21
DS -1830 570 -2185 570 39 21
DS -1830 826 -1830 -826 39 21
DS -1830 -826 1830 -826 39 21
DS 1830 -826 1830 826 39 21
DS 1830 826 -1830 826 39 21
DS 2185 570 2185 -570 39 21
DS 2185 -570 1830 -570 39 21
DS 1830 -570 1830 570 39 21
DS 1830 570 2185 570 39 21
$EndMODULE DF9-11-1V
$MODULE DF9-13-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-13-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-13-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 275 819 0 0 0
At SMD N 00888000
Po -1180 771
$EndPAD
$PAD
Sh "2" R 276 819 0 0 0
At SMD N 00888000
Po -787 771
$EndPAD
$PAD
Sh "3" R 276 819 0 0 0
At SMD N 00888000
Po -393 771
$EndPAD
$PAD
Sh "4" R 274 819 0 0 0
At SMD N 00888000
Po 0 771
$EndPAD
$PAD
Sh "5" R 276 819 0 0 0
At SMD N 00888000
Po 393 771
$EndPAD
$PAD
Sh "6" R 276 819 0 0 0
At SMD N 00888000
Po 787 771
$EndPAD
$PAD
Sh "7" R 275 819 0 0 0
At SMD N 00888000
Po 1180 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -2303 0
$EndPAD
$PAD
Sh "8" R 276 819 0 0 0
At SMD N 00888000
Po 984 -771
$EndPAD
$PAD
Sh "9" R 276 819 0 0 0
At SMD N 00888000
Po 590 -771
$EndPAD
$PAD
Sh "10" R 275 819 0 0 0
At SMD N 00888000
Po 196 -771
$EndPAD
$PAD
Sh "11" R 275 819 0 0 0
At SMD N 00888000
Po -196 -771
$EndPAD
$PAD
Sh "12" R 276 819 0 0 0
At SMD N 00888000
Po -590 -771
$EndPAD
$PAD
Sh "13" R 276 819 0 0 0
At SMD N 00888000
Po -984 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 2303 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -1791 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 1791 0
$EndPAD
DS -2027 826 -2027 -826 39 21
DS -2027 -826 2027 -826 39 21
DS 2027 -826 2027 826 39 21
DS 2027 826 -2027 826 39 21
DS -2381 570 -2381 -570 39 21
DS -2381 -570 -2027 -570 39 21
DS -2027 -570 -2027 570 39 21
DS -2027 570 -2381 570 39 21
DS -2027 826 -2027 -826 39 21
DS -2027 -826 2027 -826 39 21
DS 2027 -826 2027 826 39 21
DS 2027 826 -2027 826 39 21
DS 2381 570 2381 -570 39 21
DS 2381 -570 2027 -570 39 21
DS 2027 -570 2027 570 39 21
DS 2027 570 2381 570 39 21
$EndMODULE DF9-13-1V
$MODULE DF9-15-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-15-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-15-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 275 819 0 0 0
At SMD N 00888000
Po -1377 771
$EndPAD
$PAD
Sh "2" R 276 819 0 0 0
At SMD N 00888000
Po -984 771
$EndPAD
$PAD
Sh "3" R 276 819 0 0 0
At SMD N 00888000
Po -590 771
$EndPAD
$PAD
Sh "4" R 275 819 0 0 0
At SMD N 00888000
Po -196 771
$EndPAD
$PAD
Sh "5" R 275 819 0 0 0
At SMD N 00888000
Po 196 771
$EndPAD
$PAD
Sh "6" R 276 819 0 0 0
At SMD N 00888000
Po 590 771
$EndPAD
$PAD
Sh "7" R 276 819 0 0 0
At SMD N 00888000
Po 984 771
$EndPAD
$PAD
Sh "8" R 275 819 0 0 0
At SMD N 00888000
Po 1377 771
$EndPAD
$PAD
Sh "S1" R 511 1338 0 0 0
At SMD N 00888000
Po -2499 0
$EndPAD
$PAD
Sh "9" R 275 819 0 0 0
At SMD N 00888000
Po 1180 -771
$EndPAD
$PAD
Sh "10" R 276 819 0 0 0
At SMD N 00888000
Po 787 -771
$EndPAD
$PAD
Sh "11" R 276 819 0 0 0
At SMD N 00888000
Po 393 -771
$EndPAD
$PAD
Sh "12" R 274 819 0 0 0
At SMD N 00888000
Po 0 -771
$EndPAD
$PAD
Sh "13" R 276 819 0 0 0
At SMD N 00888000
Po -393 -771
$EndPAD
$PAD
Sh "14" R 276 819 0 0 0
At SMD N 00888000
Po -787 -771
$EndPAD
$PAD
Sh "15" R 275 819 0 0 0
At SMD N 00888000
Po -1180 -771
$EndPAD
$PAD
Sh "S2" R 511 1338 0 0 0
At SMD N 00888000
Po 2499 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -1988 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 1988 0
$EndPAD
DS -2224 826 -2224 -826 39 21
DS -2224 -826 2224 -826 39 21
DS 2224 -826 2224 826 39 21
DS 2224 826 -2224 826 39 21
DS -2578 570 -2578 -570 39 21
DS -2578 -570 -2224 -570 39 21
DS -2224 -570 -2224 570 39 21
DS -2224 570 -2578 570 39 21
DS -2224 826 -2224 -826 39 21
DS -2224 -826 2224 -826 39 21
DS 2224 -826 2224 826 39 21
DS 2224 826 -2224 826 39 21
DS 2578 570 2578 -570 39 21
DS 2578 -570 2224 -570 39 21
DS 2224 -570 2224 570 39 21
DS 2224 570 2578 570 39 21
$EndMODULE DF9-15-1V
$MODULE DF9-17-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-17-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-17-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 275 819 0 0 0
At SMD N 00888000
Po -1574 771
$EndPAD
$PAD
Sh "2" R 275 819 0 0 0
At SMD N 00888000
Po -1180 771
$EndPAD
$PAD
Sh "3" R 276 819 0 0 0
At SMD N 00888000
Po -787 771
$EndPAD
$PAD
Sh "4" R 276 819 0 0 0
At SMD N 00888000
Po -393 771
$EndPAD
$PAD
Sh "5" R 274 819 0 0 0
At SMD N 00888000
Po 0 771
$EndPAD
$PAD
Sh "6" R 276 819 0 0 0
At SMD N 00888000
Po 393 771
$EndPAD
$PAD
Sh "7" R 276 819 0 0 0
At SMD N 00888000
Po 787 771
$EndPAD
$PAD
Sh "8" R 275 819 0 0 0
At SMD N 00888000
Po 1180 771
$EndPAD
$PAD
Sh "9" R 275 819 0 0 0
At SMD N 00888000
Po 1574 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -2696 0
$EndPAD
$PAD
Sh "10" R 275 819 0 0 0
At SMD N 00888000
Po 1377 -771
$EndPAD
$PAD
Sh "11" R 276 819 0 0 0
At SMD N 00888000
Po 984 -771
$EndPAD
$PAD
Sh "12" R 276 819 0 0 0
At SMD N 00888000
Po 590 -771
$EndPAD
$PAD
Sh "13" R 275 819 0 0 0
At SMD N 00888000
Po 196 -771
$EndPAD
$PAD
Sh "14" R 275 819 0 0 0
At SMD N 00888000
Po -196 -771
$EndPAD
$PAD
Sh "15" R 276 819 0 0 0
At SMD N 00888000
Po -590 -771
$EndPAD
$PAD
Sh "16" R 276 819 0 0 0
At SMD N 00888000
Po -984 -771
$EndPAD
$PAD
Sh "17" R 275 819 0 0 0
At SMD N 00888000
Po -1377 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 2696 0
$EndPAD
$PAD
Sh "HOLE" C 511 511 0 0 0
Dr 511 0 0
At HOLE N 00C00000
Po -2184 0
$EndPAD
$PAD
Sh "HOLE" C 511 511 0 0 0
Dr 511 0 0
At HOLE N 00C00000
Po 2184 0
$EndPAD
DS -2421 826 -2421 -826 39 21
DS -2421 -826 2421 -826 39 21
DS 2421 -826 2421 826 39 21
DS 2421 826 -2421 826 39 21
DS -2775 570 -2775 -570 39 21
DS -2775 -570 -2421 -570 39 21
DS -2421 -570 -2421 570 39 21
DS -2421 570 -2775 570 39 21
DS -2421 826 -2421 -826 39 21
DS -2421 -826 2421 -826 39 21
DS 2421 -826 2421 826 39 21
DS 2421 826 -2421 826 39 21
DS 2775 570 2775 -570 39 21
DS 2775 -570 2421 -570 39 21
DS 2421 -570 2421 570 39 21
DS 2421 570 2775 570 39 21
$EndMODULE DF9-17-1V
$MODULE DF9-19-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-19-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-19-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 819 0 0 0
At SMD N 00888000
Po -1771 771
$EndPAD
$PAD
Sh "2" R 275 819 0 0 0
At SMD N 00888000
Po -1377 771
$EndPAD
$PAD
Sh "3" R 276 819 0 0 0
At SMD N 00888000
Po -984 771
$EndPAD
$PAD
Sh "4" R 276 819 0 0 0
At SMD N 00888000
Po -590 771
$EndPAD
$PAD
Sh "5" R 275 819 0 0 0
At SMD N 00888000
Po -196 771
$EndPAD
$PAD
Sh "6" R 275 819 0 0 0
At SMD N 00888000
Po 196 771
$EndPAD
$PAD
Sh "7" R 276 819 0 0 0
At SMD N 00888000
Po 590 771
$EndPAD
$PAD
Sh "8" R 276 819 0 0 0
At SMD N 00888000
Po 984 771
$EndPAD
$PAD
Sh "9" R 275 819 0 0 0
At SMD N 00888000
Po 1377 771
$EndPAD
$PAD
Sh "10" R 276 819 0 0 0
At SMD N 00888000
Po 1771 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -2893 0
$EndPAD
$PAD
Sh "11" R 275 819 0 0 0
At SMD N 00888000
Po 1574 -771
$EndPAD
$PAD
Sh "12" R 275 819 0 0 0
At SMD N 00888000
Po 1180 -771
$EndPAD
$PAD
Sh "13" R 276 819 0 0 0
At SMD N 00888000
Po 787 -771
$EndPAD
$PAD
Sh "14" R 276 819 0 0 0
At SMD N 00888000
Po 393 -771
$EndPAD
$PAD
Sh "15" R 274 819 0 0 0
At SMD N 00888000
Po 0 -771
$EndPAD
$PAD
Sh "16" R 276 819 0 0 0
At SMD N 00888000
Po -393 -771
$EndPAD
$PAD
Sh "17" R 276 819 0 0 0
At SMD N 00888000
Po -787 -771
$EndPAD
$PAD
Sh "18" R 275 819 0 0 0
At SMD N 00888000
Po -1180 -771
$EndPAD
$PAD
Sh "19" R 275 819 0 0 0
At SMD N 00888000
Po -1574 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 2893 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -2381 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 2381 0
$EndPAD
DS -2618 826 -2618 -826 39 21
DS -2618 -826 2618 -826 39 21
DS 2618 -826 2618 826 39 21
DS 2618 826 -2618 826 39 21
DS -2972 570 -2972 -570 39 21
DS -2972 -570 -2618 -570 39 21
DS -2618 -570 -2618 570 39 21
DS -2618 570 -2972 570 39 21
DS -2618 826 -2618 -826 39 21
DS -2618 -826 2618 -826 39 21
DS 2618 -826 2618 826 39 21
DS 2618 826 -2618 826 39 21
DS 2972 570 2972 -570 39 21
DS 2972 -570 2618 -570 39 21
DS 2618 -570 2618 570 39 21
DS 2618 570 2972 570 39 21
$EndMODULE DF9-19-1V
$MODULE DF9-21-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-21-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-21-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 819 0 0 0
At SMD N 00888000
Po -1968 771
$EndPAD
$PAD
Sh "2" R 275 819 0 0 0
At SMD N 00888000
Po -1574 771
$EndPAD
$PAD
Sh "3" R 275 819 0 0 0
At SMD N 00888000
Po -1180 771
$EndPAD
$PAD
Sh "4" R 276 819 0 0 0
At SMD N 00888000
Po -787 771
$EndPAD
$PAD
Sh "5" R 276 819 0 0 0
At SMD N 00888000
Po -393 771
$EndPAD
$PAD
Sh "6" R 274 819 0 0 0
At SMD N 00888000
Po 0 771
$EndPAD
$PAD
Sh "7" R 276 819 0 0 0
At SMD N 00888000
Po 393 771
$EndPAD
$PAD
Sh "8" R 276 819 0 0 0
At SMD N 00888000
Po 787 771
$EndPAD
$PAD
Sh "9" R 275 819 0 0 0
At SMD N 00888000
Po 1180 771
$EndPAD
$PAD
Sh "10" R 275 819 0 0 0
At SMD N 00888000
Po 1574 771
$EndPAD
$PAD
Sh "11" R 276 819 0 0 0
At SMD N 00888000
Po 1968 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -3090 0
$EndPAD
$PAD
Sh "12" R 276 819 0 0 0
At SMD N 00888000
Po 1771 -771
$EndPAD
$PAD
Sh "13" R 275 819 0 0 0
At SMD N 00888000
Po 1377 -771
$EndPAD
$PAD
Sh "14" R 276 819 0 0 0
At SMD N 00888000
Po 984 -771
$EndPAD
$PAD
Sh "15" R 276 819 0 0 0
At SMD N 00888000
Po 590 -771
$EndPAD
$PAD
Sh "16" R 275 819 0 0 0
At SMD N 00888000
Po 196 -771
$EndPAD
$PAD
Sh "17" R 275 819 0 0 0
At SMD N 00888000
Po -196 -771
$EndPAD
$PAD
Sh "18" R 276 819 0 0 0
At SMD N 00888000
Po -590 -771
$EndPAD
$PAD
Sh "19" R 276 819 0 0 0
At SMD N 00888000
Po -984 -771
$EndPAD
$PAD
Sh "20" R 275 819 0 0 0
At SMD N 00888000
Po -1377 -771
$EndPAD
$PAD
Sh "21" R 276 819 0 0 0
At SMD N 00888000
Po -1771 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 3090 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -2578 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 2578 0
$EndPAD
DS -2814 826 -2814 -826 39 21
DS -2814 -826 2814 -826 39 21
DS 2814 -826 2814 826 39 21
DS 2814 826 -2814 826 39 21
DS -3169 570 -3169 -570 39 21
DS -3169 -570 -2814 -570 39 21
DS -2814 -570 -2814 570 39 21
DS -2814 570 -3169 570 39 21
DS -2814 826 -2814 -826 39 21
DS -2814 -826 2814 -826 39 21
DS 2814 -826 2814 826 39 21
DS 2814 826 -2814 826 39 21
DS 3169 570 3169 -570 39 21
DS 3169 -570 2814 -570 39 21
DS 2814 -570 2814 570 39 21
DS 2814 570 3169 570 39 21
$EndMODULE DF9-21-1V
$MODULE DF9-23-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-23-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-23-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 819 0 0 0
At SMD N 00888000
Po -2165 771
$EndPAD
$PAD
Sh "2" R 276 819 0 0 0
At SMD N 00888000
Po -1771 771
$EndPAD
$PAD
Sh "3" R 275 819 0 0 0
At SMD N 00888000
Po -1377 771
$EndPAD
$PAD
Sh "4" R 276 819 0 0 0
At SMD N 00888000
Po -984 771
$EndPAD
$PAD
Sh "5" R 276 819 0 0 0
At SMD N 00888000
Po -590 771
$EndPAD
$PAD
Sh "6" R 275 819 0 0 0
At SMD N 00888000
Po -196 771
$EndPAD
$PAD
Sh "7" R 275 819 0 0 0
At SMD N 00888000
Po 196 771
$EndPAD
$PAD
Sh "8" R 276 819 0 0 0
At SMD N 00888000
Po 590 771
$EndPAD
$PAD
Sh "9" R 276 819 0 0 0
At SMD N 00888000
Po 984 771
$EndPAD
$PAD
Sh "10" R 275 819 0 0 0
At SMD N 00888000
Po 1377 771
$EndPAD
$PAD
Sh "11" R 276 819 0 0 0
At SMD N 00888000
Po 1771 771
$EndPAD
$PAD
Sh "12" R 276 819 0 0 0
At SMD N 00888000
Po 2165 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -3287 0
$EndPAD
$PAD
Sh "13" R 276 819 0 0 0
At SMD N 00888000
Po 1968 -771
$EndPAD
$PAD
Sh "14" R 275 819 0 0 0
At SMD N 00888000
Po 1574 -771
$EndPAD
$PAD
Sh "15" R 275 819 0 0 0
At SMD N 00888000
Po 1180 -771
$EndPAD
$PAD
Sh "16" R 276 819 0 0 0
At SMD N 00888000
Po 787 -771
$EndPAD
$PAD
Sh "17" R 276 819 0 0 0
At SMD N 00888000
Po 393 -771
$EndPAD
$PAD
Sh "18" R 274 819 0 0 0
At SMD N 00888000
Po 0 -771
$EndPAD
$PAD
Sh "19" R 276 819 0 0 0
At SMD N 00888000
Po -393 -771
$EndPAD
$PAD
Sh "20" R 276 819 0 0 0
At SMD N 00888000
Po -787 -771
$EndPAD
$PAD
Sh "21" R 275 819 0 0 0
At SMD N 00888000
Po -1180 -771
$EndPAD
$PAD
Sh "22" R 275 819 0 0 0
At SMD N 00888000
Po -1574 -771
$EndPAD
$PAD
Sh "23" R 276 819 0 0 0
At SMD N 00888000
Po -1968 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 3287 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -2775 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 2775 0
$EndPAD
DS -3011 826 -3011 -826 39 21
DS -3011 -826 3011 -826 39 21
DS 3011 -826 3011 826 39 21
DS 3011 826 -3011 826 39 21
DS -3366 570 -3366 -570 39 21
DS -3366 -570 -3011 -570 39 21
DS -3011 -570 -3011 570 39 21
DS -3011 570 -3366 570 39 21
DS -3011 826 -3011 -826 39 21
DS -3011 -826 3011 -826 39 21
DS 3011 -826 3011 826 39 21
DS 3011 826 -3011 826 39 21
DS 3366 570 3366 -570 39 21
DS 3366 -570 3011 -570 39 21
DS 3011 -570 3011 570 39 21
DS 3011 570 3366 570 39 21
$EndMODULE DF9-23-1V
$MODULE DF9-25-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-25-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-25-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 819 0 0 0
At SMD N 00888000
Po -2362 771
$EndPAD
$PAD
Sh "2" R 276 819 0 0 0
At SMD N 00888000
Po -1968 771
$EndPAD
$PAD
Sh "3" R 275 819 0 0 0
At SMD N 00888000
Po -1574 771
$EndPAD
$PAD
Sh "4" R 275 819 0 0 0
At SMD N 00888000
Po -1180 771
$EndPAD
$PAD
Sh "5" R 276 819 0 0 0
At SMD N 00888000
Po -787 771
$EndPAD
$PAD
Sh "6" R 276 819 0 0 0
At SMD N 00888000
Po -393 771
$EndPAD
$PAD
Sh "7" R 274 819 0 0 0
At SMD N 00888000
Po 0 771
$EndPAD
$PAD
Sh "8" R 276 819 0 0 0
At SMD N 00888000
Po 393 771
$EndPAD
$PAD
Sh "9" R 276 819 0 0 0
At SMD N 00888000
Po 787 771
$EndPAD
$PAD
Sh "10" R 275 819 0 0 0
At SMD N 00888000
Po 1180 771
$EndPAD
$PAD
Sh "11" R 275 819 0 0 0
At SMD N 00888000
Po 1574 771
$EndPAD
$PAD
Sh "12" R 276 819 0 0 0
At SMD N 00888000
Po 1968 771
$EndPAD
$PAD
Sh "13" R 276 819 0 0 0
At SMD N 00888000
Po 2362 771
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -3484 0
$EndPAD
$PAD
Sh "14" R 276 819 0 0 0
At SMD N 00888000
Po 2165 -771
$EndPAD
$PAD
Sh "15" R 276 819 0 0 0
At SMD N 00888000
Po 1771 -771
$EndPAD
$PAD
Sh "16" R 275 819 0 0 0
At SMD N 00888000
Po 1377 -771
$EndPAD
$PAD
Sh "17" R 276 819 0 0 0
At SMD N 00888000
Po 984 -771
$EndPAD
$PAD
Sh "18" R 276 819 0 0 0
At SMD N 00888000
Po 590 -771
$EndPAD
$PAD
Sh "19" R 275 819 0 0 0
At SMD N 00888000
Po 196 -771
$EndPAD
$PAD
Sh "20" R 275 819 0 0 0
At SMD N 00888000
Po -196 -771
$EndPAD
$PAD
Sh "21" R 276 819 0 0 0
At SMD N 00888000
Po -590 -771
$EndPAD
$PAD
Sh "22" R 276 819 0 0 0
At SMD N 00888000
Po -984 -771
$EndPAD
$PAD
Sh "23" R 275 819 0 0 0
At SMD N 00888000
Po -1377 -771
$EndPAD
$PAD
Sh "24" R 276 819 0 0 0
At SMD N 00888000
Po -1771 -771
$EndPAD
$PAD
Sh "25" R 276 819 0 0 0
At SMD N 00888000
Po -2165 -771
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 3484 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -2972 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 2972 0
$EndPAD
DS -3208 826 -3208 -826 39 21
DS -3208 -826 3208 -826 39 21
DS 3208 -826 3208 826 39 21
DS 3208 826 -3208 826 39 21
DS -3562 570 -3562 -570 39 21
DS -3562 -570 -3208 -570 39 21
DS -3208 -570 -3208 570 39 21
DS -3208 570 -3562 570 39 21
DS -3208 826 -3208 -826 39 21
DS -3208 -826 3208 -826 39 21
DS 3208 -826 3208 826 39 21
DS 3208 826 -3208 826 39 21
DS 3562 570 3562 -570 39 21
DS 3562 -570 3208 -570 39 21
DS 3208 -570 3208 570 39 21
DS 3208 570 3562 570 39 21
$EndMODULE DF9-25-1V
$MODULE DF9-31-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-31-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-31-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 276 591 0 0 0
At SMD N 00888000
Po -2952 885
$EndPAD
$PAD
Sh "2" R 275 591 0 0 0
At SMD N 00888000
Po -2558 885
$EndPAD
$PAD
Sh "3" R 276 591 0 0 0
At SMD N 00888000
Po -2165 885
$EndPAD
$PAD
Sh "4" R 276 591 0 0 0
At SMD N 00888000
Po -1771 885
$EndPAD
$PAD
Sh "5" R 275 591 0 0 0
At SMD N 00888000
Po -1377 885
$EndPAD
$PAD
Sh "6" R 276 591 0 0 0
At SMD N 00888000
Po -984 885
$EndPAD
$PAD
Sh "7" R 276 591 0 0 0
At SMD N 00888000
Po -590 885
$EndPAD
$PAD
Sh "8" R 275 591 0 0 0
At SMD N 00888000
Po -196 885
$EndPAD
$PAD
Sh "9" R 275 591 0 0 0
At SMD N 00888000
Po 196 885
$EndPAD
$PAD
Sh "10" R 276 591 0 0 0
At SMD N 00888000
Po 590 885
$EndPAD
$PAD
Sh "11" R 276 591 0 0 0
At SMD N 00888000
Po 984 885
$EndPAD
$PAD
Sh "12" R 275 591 0 0 0
At SMD N 00888000
Po 1377 885
$EndPAD
$PAD
Sh "13" R 276 591 0 0 0
At SMD N 00888000
Po 1771 885
$EndPAD
$PAD
Sh "14" R 276 591 0 0 0
At SMD N 00888000
Po 2165 885
$EndPAD
$PAD
Sh "15" R 275 591 0 0 0
At SMD N 00888000
Po 2558 885
$EndPAD
$PAD
Sh "16" R 276 591 0 0 0
At SMD N 00888000
Po 2952 885
$EndPAD
$PAD
Sh "S1" R 512 1338 0 0 0
At SMD N 00888000
Po -4074 0
$EndPAD
$PAD
Sh "17" R 275 591 0 0 0
At SMD N 00888000
Po 2755 -885
$EndPAD
$PAD
Sh "18" R 276 591 0 0 0
At SMD N 00888000
Po 2362 -885
$EndPAD
$PAD
Sh "19" R 276 591 0 0 0
At SMD N 00888000
Po 1968 -885
$EndPAD
$PAD
Sh "20" R 275 591 0 0 0
At SMD N 00888000
Po 1574 -885
$EndPAD
$PAD
Sh "21" R 275 591 0 0 0
At SMD N 00888000
Po 1180 -885
$EndPAD
$PAD
Sh "22" R 276 591 0 0 0
At SMD N 00888000
Po 787 -885
$EndPAD
$PAD
Sh "23" R 276 591 0 0 0
At SMD N 00888000
Po 393 -885
$EndPAD
$PAD
Sh "24" R 274 591 0 0 0
At SMD N 00888000
Po 0 -885
$EndPAD
$PAD
Sh "25" R 276 591 0 0 0
At SMD N 00888000
Po -393 -885
$EndPAD
$PAD
Sh "26" R 276 591 0 0 0
At SMD N 00888000
Po -787 -885
$EndPAD
$PAD
Sh "27" R 275 591 0 0 0
At SMD N 00888000
Po -1180 -885
$EndPAD
$PAD
Sh "28" R 275 591 0 0 0
At SMD N 00888000
Po -1574 -885
$EndPAD
$PAD
Sh "29" R 276 591 0 0 0
At SMD N 00888000
Po -1968 -885
$EndPAD
$PAD
Sh "30" R 276 591 0 0 0
At SMD N 00888000
Po -2362 -885
$EndPAD
$PAD
Sh "31" R 275 591 0 0 0
At SMD N 00888000
Po -2755 -885
$EndPAD
$PAD
Sh "S2" R 512 1338 0 0 0
At SMD N 00888000
Po 4074 0
$EndPAD
$PAD
Sh "HOLE" C 511 511 0 0 0
Dr 511 0 0
At HOLE N 00C00000
Po -3562 0
$EndPAD
$PAD
Sh "HOLE" C 511 511 0 0 0
Dr 511 0 0
At HOLE N 00C00000
Po 3562 0
$EndPAD
DS -3799 826 -3799 -826 39 21
DS -3799 -826 3799 -826 39 21
DS 3799 -826 3799 826 39 21
DS 3799 826 -3799 826 39 21
DS -4153 570 -4153 -570 39 21
DS -4153 -570 -3799 -570 39 21
DS -3799 -570 -3799 570 39 21
DS -3799 570 -4153 570 39 21
DS -3799 826 -3799 -826 39 21
DS -3799 -826 3799 -826 39 21
DS 3799 -826 3799 826 39 21
DS 3799 826 -3799 826 39 21
DS 4153 570 4153 -570 39 21
DS 4153 -570 3799 -570 39 21
DS 3799 -570 3799 570 39 21
DS 3799 570 4153 570 39 21
$EndMODULE DF9-31-1V
$MODULE DF9-41-1V
Po 0 0 0 15 595926E4 00000000 ~~
Li DF9-41-1V
Sc 595926E4
At SMD
Op 0 0 0
T0 0 -150 200 200 0 40 N V 25 "DF9-41-1V"
T1 0 150 200 200 0 40 N I 25 "Val*"
$PAD
Sh "1" R 275 591 0 0 0
At SMD N 00888000
Po -3936 885
$EndPAD
$PAD
Sh "2" R 276 591 0 0 0
At SMD N 00888000
Po -3543 885
$EndPAD
$PAD
Sh "3" R 276 591 0 0 0
At SMD N 00888000
Po -3149 885
$EndPAD
$PAD
Sh "4" R 275 591 0 0 0
At SMD N 00888000
Po -2755 885
$EndPAD
$PAD
Sh "5" R 276 591 0 0 0
At SMD N 00888000
Po -2362 885
$EndPAD
$PAD
Sh "6" R 276 591 0 0 0
At SMD N 00888000
Po -1968 885
$EndPAD
$PAD
Sh "7" R 275 591 0 0 0
At SMD N 00888000
Po -1574 885
$EndPAD
$PAD
Sh "8" R 275 591 0 0 0
At SMD N 00888000
Po -1180 885
$EndPAD
$PAD
Sh "9" R 276 591 0 0 0
At SMD N 00888000
Po -787 885
$EndPAD
$PAD
Sh "10" R 276 591 0 0 0
At SMD N 00888000
Po -393 885
$EndPAD
$PAD
Sh "11" R 274 591 0 0 0
At SMD N 00888000
Po 0 885
$EndPAD
$PAD
Sh "12" R 276 591 0 0 0
At SMD N 00888000
Po 393 885
$EndPAD
$PAD
Sh "13" R 276 591 0 0 0
At SMD N 00888000
Po 787 885
$EndPAD
$PAD
Sh "14" R 275 591 0 0 0
At SMD N 00888000
Po 1180 885
$EndPAD
$PAD
Sh "15" R 275 591 0 0 0
At SMD N 00888000
Po 1574 885
$EndPAD
$PAD
Sh "16" R 276 591 0 0 0
At SMD N 00888000
Po 1968 885
$EndPAD
$PAD
Sh "17" R 276 591 0 0 0
At SMD N 00888000
Po 2362 885
$EndPAD
$PAD
Sh "18" R 275 591 0 0 0
At SMD N 00888000
Po 2755 885
$EndPAD
$PAD
Sh "19" R 276 591 0 0 0
At SMD N 00888000
Po 3149 885
$EndPAD
$PAD
Sh "20" R 276 591 0 0 0
At SMD N 00888000
Po 3543 885
$EndPAD
$PAD
Sh "21" R 275 591 0 0 0
At SMD N 00888000
Po 3936 885
$EndPAD
$PAD
Sh "S1" R 511 1338 0 0 0
At SMD N 00888000
Po -5058 0
$EndPAD
$PAD
Sh "22" R 275 591 0 0 0
At SMD N 00888000
Po 3739 -885
$EndPAD
$PAD
Sh "23" R 276 591 0 0 0
At SMD N 00888000
Po 3346 -885
$EndPAD
$PAD
Sh "24" R 276 591 0 0 0
At SMD N 00888000
Po 2952 -885
$EndPAD
$PAD
Sh "25" R 275 591 0 0 0
At SMD N 00888000
Po 2558 -885
$EndPAD
$PAD
Sh "26" R 276 591 0 0 0
At SMD N 00888000
Po 2165 -885
$EndPAD
$PAD
Sh "27" R 276 591 0 0 0
At SMD N 00888000
Po 1771 -885
$EndPAD
$PAD
Sh "28" R 275 591 0 0 0
At SMD N 00888000
Po 1377 -885
$EndPAD
$PAD
Sh "29" R 276 591 0 0 0
At SMD N 00888000
Po 984 -885
$EndPAD
$PAD
Sh "30" R 276 591 0 0 0
At SMD N 00888000
Po 590 -885
$EndPAD
$PAD
Sh "31" R 275 591 0 0 0
At SMD N 00888000
Po 196 -885
$EndPAD
$PAD
Sh "32" R 275 591 0 0 0
At SMD N 00888000
Po -196 -885
$EndPAD
$PAD
Sh "33" R 276 591 0 0 0
At SMD N 00888000
Po -590 -885
$EndPAD
$PAD
Sh "34" R 276 591 0 0 0
At SMD N 00888000
Po -984 -885
$EndPAD
$PAD
Sh "35" R 275 591 0 0 0
At SMD N 00888000
Po -1377 -885
$EndPAD
$PAD
Sh "36" R 276 591 0 0 0
At SMD N 00888000
Po -1771 -885
$EndPAD
$PAD
Sh "37" R 276 591 0 0 0
At SMD N 00888000
Po -2165 -885
$EndPAD
$PAD
Sh "38" R 275 591 0 0 0
At SMD N 00888000
Po -2558 -885
$EndPAD
$PAD
Sh "39" R 276 591 0 0 0
At SMD N 00888000
Po -2952 -885
$EndPAD
$PAD
Sh "40" R 276 591 0 0 0
At SMD N 00888000
Po -3346 -885
$EndPAD
$PAD
Sh "41" R 275 591 0 0 0
At SMD N 00888000
Po -3739 -885
$EndPAD
$PAD
Sh "S2" R 511 1338 0 0 0
At SMD N 00888000
Po 5058 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po -4547 0
$EndPAD
$PAD
Sh "HOLE" O 512 510 0 0 0
Dr 512 0 0 O 512 510
At HOLE N 00C00000
Po 4547 0
$EndPAD
DS -4783 826 -4783 -826 39 21
DS -4783 -826 4783 -826 39 21
DS 4783 -826 4783 826 39 21
DS 4783 826 -4783 826 39 21
DS -5137 570 -5137 -570 39 21
DS -5137 -570 -4783 -570 39 21
DS -4783 -570 -4783 570 39 21
DS -4783 570 -5137 570 39 21
DS -4783 826 -4783 -826 39 21
DS -4783 -826 4783 -826 39 21
DS 4783 -826 4783 826 39 21
DS 4783 826 -4783 826 39 21
DS 5137 570 5137 -570 39 21
DS 5137 -570 4783 -570 39 21
DS 4783 -570 4783 570 39 21
DS 4783 570 5137 570 39 21
$EndMODULE DF9-41-1V
$EndLIBRARY
